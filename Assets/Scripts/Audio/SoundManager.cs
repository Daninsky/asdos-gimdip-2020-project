﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Walking
{
    
    // SoundManager class for playing music
    // AssetHandler used to hold audio files
    public static class SoundManager
    {
        public static void GangnamStyle()
        {
            GameObject soundGameObject = new GameObject("Sound");
            AudioSource audioSource = soundGameObject.AddComponent<AudioSource>();
            audioSource.PlayOneShot(AssetHandler.instance.gangnamStyle);
        }
    }
}

