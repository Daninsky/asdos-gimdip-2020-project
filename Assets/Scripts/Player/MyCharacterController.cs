﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECM.Controllers;

namespace Walking
{
    public class MyCharacterController : BaseCharacterController
    {
        protected override void Animate()
        {
            // Add animator related code here...

            Animator playerAnim = this.transform.GetChild(1).gameObject.GetComponent<Animator>();

            playerAnim.SetFloat("Forward", Input.GetAxisRaw("Vertical"));
            playerAnim.SetFloat("Turn", Input.GetAxisRaw("Horizontal"));

        }

        protected override void HandleInput()
        {
            // Default ECM Input as used in BaseCharacterController HandleInput method.
            // Replace this with your custom input code here...

            // Toggle pause / resume.
            // By default, will restore character's velocity on resume (eg: restoreVelocityOnResume = true)

            if (Input.GetKeyDown(KeyCode.P))
                pause = !pause;

            // Handle user input

            moveDirection = new Vector3
            {
                x = Input.GetAxisRaw("Horizontal"),
                y = 0.0f,   
                z = Input.GetAxisRaw("Vertical")
            };

            moveDirection = Quaternion.Euler(0, Camera.main.transform.eulerAngles.y, 0) * moveDirection;

            jump = Input.GetButton("Jump");

            crouch = Input.GetKey(KeyCode.C);


            //
            // For example, you can replace the above code to use the Unity NEW Input system as follows

            //var gamepad = Gamepad.current;
            //if (gamepad == null)
            //    return; // No gamepad connected.

            //moveDirection = gamepad.leftStick.ReadValue();

            //... Add gamepad button inputs here
            //etc...


            //
            // Example using Eeasy Touch to add movile input

            // Easy touch input

            //moveDirection = new Vector3
            //{
            //    x = ETCInput.GetAxis("Left_Horizontal"),
            //    y = 0f,
            //    z = ETCInput.GetAxis("Left_Vertical")
            //};

            //jump = ETCInput.GetButton("Jump");
        }


    }
}
