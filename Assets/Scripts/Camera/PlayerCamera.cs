﻿using System.Collections;
using UnityEngine;
using UnityEngine.UIElements;
using Walking;

public class PlayerCamera : MonoBehaviour
{

    #region Serialized Private Fields

    [Header("Target Player")]

    // the target player
    [SerializeField] private Transform player;

    [Header("Camera Settings")]

    // camera smoothing speed
    [SerializeField] private float smoothSpeed;

    // camera offset
    [SerializeField] private Vector3 offset;

    // camera drag speed
    [SerializeField] private float dragSpeed;

    #endregion

    #region Private Fields
    private Vector3 dragOrigin;

    private bool isMoving;
    private Vector3 currentPosition;
    private Vector3 previousPosition;

    private float rotY = 0.0f;
    private float rotX = 0.0f;
    #endregion

    #region Public Fields
    public Transform Player { get => player; private set => player = value; }
    public float SmoothSpeed { get => smoothSpeed; private set => smoothSpeed = value; }
    public Vector3 Offset { get => offset; private set => offset = value; }
    public float DragSpeed { get => dragSpeed; private set => dragSpeed = value; }

    #endregion

    #region Monobehaviour Callbacks

    private void Start()
    {
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;

        offset = new Vector3(player.position.x, player.position.y + 2.5f, player.position.z - 2.0f);
    }

    private void LateUpdate()
    {
        //CheckIfCameraIsMoving();
        //RotateCamera();

        // MOVING CAMERA ON DRAG
        //if (Input.GetMouseButtonDown(0))
        //{
        //    dragOrigin = Input.mousePosition;
        //    return;
        //}

        //if (!Input.GetMouseButton(0)) return;

        //Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        //Vector3 move = new Vector3(pos.x * dragSpeed, 0, pos.y * dragSpeed);

        //transform.Translate(move, Space.World);

        // FIRST PERSON CAMERA

        //float mouseX = Input.GetAxis("Mouse X");
        //float mouseY = -Input.GetAxis("Mouse Y");

        //rotY += mouseX * dragSpeed * Time.deltaTime;
        //rotX += mouseY * dragSpeed * Time.deltaTime;  

        //float clampAngle = 80f;
        //rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

        //Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
        //transform.rotation = localRotation;


        // NORMAL ROTATING CAMERA 

        offset = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * dragSpeed, Vector3.up) * Quaternion.AngleAxis(Input.GetAxis("Mouse Y") * (dragSpeed / 2), Vector3.left) * offset;
        transform.position = player.position + offset;
        transform.LookAt(player.position + new Vector3(0,1,0));

        //player.transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y, 0);
    }
      

    //private void LateUpdate()
    //{
    //    if (player.GetComponent<CharacterPickup>().weedCount > 0) 
    //    {
    //        StartCoroutine(nameof(BobbingCamera));
    //    }
    //    else
    //    {
    //        StartCoroutine(nameof(NormalCamera));
    //    }
        

    //    //transform.LookAt(player);
    //}


    #endregion

    


    #region Private Methods
    void CheckIfCameraIsMoving()
    {
        currentPosition = transform.position;
        if (Vector3.Distance(currentPosition, player.position + offset) < 1.5f && player.GetComponent<Rigidbody>().velocity.magnitude < 0.5)
            isMoving = false;
        else isMoving = true;

        previousPosition = currentPosition;
    }
    #endregion

    #region Private Coroutines

    private IEnumerator NormalCamera()
    {
        Vector3 targetPosition = player.position + offset;
        Vector3 smoothedhPosition = Vector3.Lerp(transform.position, targetPosition, SmoothSpeed * Time.deltaTime);
        transform.position = smoothedhPosition;
        yield return new WaitForSeconds(.1f);
    }

    private IEnumerator BobbingCamera()
    {
        Vector3 bob = player.position + new Vector3(0, 2, -8);

        yield return NormalCamera();

        if (transform.position != bob && !isMoving)
        {
            transform.position = bob;
        }

        yield return new WaitForSeconds(.1f);
    }

    private IEnumerator WaitTwoSeconds()
    {
        yield return new WaitForSeconds(2f);
    }


    #endregion

}
