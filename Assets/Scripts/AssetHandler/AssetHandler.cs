﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Walking
{

    // AssetHandler class for loading assets on runtime

    public class AssetHandler : MonoBehaviour
    {

        #region Singleton

        private static AssetHandler _instance;

        public static AssetHandler instance
        {
            get
            {
                if (_instance == null) _instance = Instantiate(Resources.Load<AssetHandler>("AssetHandler"));
                return _instance;
            }
        }

        #endregion

        #region Public Asset Variables

        public AudioClip gangnamStyle;

        #endregion
    }

}
