# **asdos-gimdip-2020-project**
## Belom ada judulnya

### Ceritanya asdos gimdip Fasilkom UI 2020 pengen belajar Unity 

### Contributors
- Fardhan Dhiadribratha Sudjono
- Andrew Savero Ongko
- Gusti Ngurah Yama Adi Putra
- Raihansyah Attallah Andrian
- Rafif Taris

### Game Design Document
(https://docs.google.com/document/d/1jRoEq9fxfQvufZ-E3f_T28Bw4WALeiJxQYad-jm_he0/edit?usp=sharing)

### How to Import and Run
1. [Install Unity, preferably with Unity Hub](https://unity3d.com/get-unity/download) (version used in latest build is 2019.4.10f1)
2. Add Project from Unity Hub
3. Run

### Assets Used

1. [Easy Character Movement](https://assetstore.unity.com/packages/templates/systems/easy-character-movement-57985)