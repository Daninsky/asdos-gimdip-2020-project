﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Walking
{
    public class CharacterPickup : MonoBehaviour
    {

        public int weedCount;
        public Animator characterAnim;
        public bool colliding = false;

        private void Start()
        {
            characterAnim = transform.GetChild(1).GetComponent<Animator>();
        }

   //      void OnGUI()
   //  	{
   //       if (hasCollided == true)
   //  		{    
   //      		GUI.Box(Rect(140,Screen.height-50,Screen.width-300,120),(labelText));
   //  		}
 		// }


        void OnGUI()
        {
        	if(colliding == true)
        	{
        		GUI.Label(new Rect(10, 10, 100, 50), "Press F to Interact with the object"); //Display
        	}
        }

        private void OnTriggerEnter(Collider other)
        {
        	if(other.tag == "Weed")
        	{
        		colliding = true;
        		Debug.Log(colliding);
        	}
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.tag == "Weed" && colliding == true && Input.GetKeyDown(KeyCode.F))
            {
	    		Debug.Log("F key is pressed");
				Destroy(other.gameObject);
				colliding = false;
	            print("Pickup Weed");
	            weedCount++;
	            SoundManager.GangnamStyle();
	            characterAnim.SetTrigger("weed");
            }
        }

        private void OnTriggerExit(Collider other)
        {
        	colliding = false;
        	Debug.Log(colliding);
        }
    }
}

